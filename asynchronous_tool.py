import asyncio
from connection_pool import ConnectionPool

class AsyncTestingTool:
    def __init__(self):
        self.cp = ConnectionPool(5, 20)

    async def action_on_connection(self):
        connection = self.cp.get_connection()
        if connection:
            connection.add_data()
            await asyncio.sleep(2)
            self.cp.release_connection(connection)

    async def clean_up_after_delay(self, delay):
        await asyncio.sleep(delay)
        self.cp.clean_up_pool()

    async def run_test(self):
        tasks = [self.action_on_connection() for _ in range(21)]
        cleanup_task = self.clean_up_after_delay(10)

        await asyncio.gather(*tasks, cleanup_task)


tool = AsyncTestingTool()

async def main():
    print(f'Conns in pool before = {len(tool.cp.pool)}')
    await tool.run_test()
    print(f'Conns in pool after = {len(tool.cp.pool)}')

    for conn in tool.cp.pool:
        print(conn.conn_id, conn.active)


if __name__ == "__main__":
    while True:
        asyncio.run(main())
