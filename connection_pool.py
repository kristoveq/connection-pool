import threading
from postgre_db import DatabaseHandler


class ConnectionPool:
    def __init__(self, min_connections, max_connections):
        self.min_connections = min_connections
        self.max_connections = max_connections
        self.lock = threading.Lock()
        self.pool = []

        with self.lock:
            for counter in range(self.min_connections):
                connection = self.create_connection()
                self.pool.append(connection)

    def create_connection(self):
        connection = DatabaseHandler(len(self.pool) + 1)
        connection.open_connection()
        print(f'Connection no.{connection.conn_id} is added to pool.')
        return connection

    def get_connection(self):
        with self.lock:
            connection = next((conn for conn in self.pool if not conn.active), None)
            if connection is not None:
                connection.active = True
                return connection
            elif len(self.pool) < self.max_connections:
                additional_conn = self.create_connection()
                self.pool.append(additional_conn)
                connection = self.pool[-1]
                connection.active = True
                return connection
            else:
                print(f'Reach limit ({self.max_connections}) of connections.')

    def release_connection(self, connection):
        with self.lock:
            if len(self.pool) <= self.max_connections:
                connection.active = False

    def clean_up_pool(self):
        with self.lock:
            if len(self.pool) <= self.min_connections:
                return None
            for single_conn in list(self.pool):
                if not single_conn.active and len(self.pool) > self.min_connections:
                    single_conn.close_connection()
                    self.pool.remove(single_conn)
                    print(f'CLEANED: CONNECTION ID: {single_conn.conn_id}')
