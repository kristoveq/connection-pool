import configparser
import os.path
import psycopg2
from psycopg2 import Error


config = configparser.ConfigParser()
config['CONNECTION_POOL'] = {
    'host': '127.0.0.1',
    'port': '5000',
    'user': 'postgres',
    'password': 'junior',
    'database': 'cp'
}
with open('connection_pool_database.ini', 'w') as config_file:
    config.write(config_file)


def config(filename='connection_pool_database.ini', section='CONNECTION_POOL'):
    current_path = os.path.dirname(os.path.abspath(__file__))
    full_path = os.path.join(current_path, filename)
    parser = configparser.ConfigParser()
    parser.read(full_path)

    database = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            database[param[0]] = param[1]
    else:
        raise Exception(f'Section {section} has not found in the file: {filename}.')
    return database


class DatabaseHandler:
    def __init__(self, conn_id):
        self.params = None
        self.connect = None
        self.cursor = None
        self.active = False
        self.conn_id = conn_id

    def open_connection(self):
        try:
            self.params = config()
            self.connect = psycopg2.connect(**self.params)
            self.connect.autocommit = False
            self.cursor = self.connect.cursor()
            self.active = False
        except (Exception, Error) as error:
            print(f'Connection to database failed: \n{error}')

    def close_connection(self):
        if self.connect is not None:
            try:
                self.cursor.close()
                self.connect.close()
            except (Exception, Error) as error:
                print(f'Can\'t close connection to database: \n{error}')

    def add_data(self):
        try:
            query = 'INSERT INTO data (name) VALUES (%s)'
            value = (f'Connection no. {self.conn_id}',)
            self.cursor.execute(query, value)
            self.connect.commit()

        except (Exception, Error) as e:
            print(f'Unable to add data: \n{e}')
